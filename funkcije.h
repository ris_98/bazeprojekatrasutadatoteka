#ifndef FUNKCIJE_H_INCLUDED
#define FUNKCIJE_H_INCLUDED
#include "stdio.h"
#include "stdlib.h"
#include "math.h"  //pozovi -lm kad kompajliras uvek zaboravis!!!!!!!
#include "preklapanje.h"  //hash funkcija metod preklapanja
#include "struktura.h"
#include "string.h"
#include "unistd.h"
#define B 7   //broj baketa
#define p 9   //broj cifara kljuca, u ovom slucaju 9 evidencioni broj
#define n 1   //broj cifara relativne adrese
#define q 4   //donje celo p/2n
#define v 10  //osnova brojnog sistema
#define fb 5 //faktor baketiranja






void kreirajZonuPrekoracenja(char* naziv){
    
    char ime[22];
    strcpy(ime,naziv);
    strcat(ime,"-zonaprekoracenja");
    
    FILE * f = fopen(ime,"wb+");
    int* e = (int *)malloc(sizeof(int));
    *e=0;
    fwrite(e,sizeof(int),1,f);
    fclose(f);



}


void formirajDatoteku(char* naziv){
    FILE *f= fopen(naziv,"wb+");
    kreirajZonuPrekoracenja(naziv);
    rewind(f);
    for(int i=0; i<B;i++){
         Baket * b = (Baket *)malloc(sizeof(Baket));
         rewind(f); //oov
         fseek(f,i*sizeof(Baket),SEEK_CUR);
         for(int j=0;j<fb;j++){
         b->nizSlogova[j].ia=0;
         b->nizSlogova[j].ebr=-1;
         strcpy(b->nizSlogova[j].id, "xxxxxxxxxxx");
         strcpy(b->nizSlogova[j].dateTime,"01.01.1974. 11:11");
         strcpy(b->nizSlogova[j].type,"xxxxxxxx");
         b->nizSlogova[j].trajanje=-1;
         b->adresa=-1;
         }
         
         fwrite(b,sizeof(Baket),1,f);
         free(b);
       


    }
    
    fclose(f);
    
}

int trazenjeSloga(int ev, char* naziv){
    //printf("Alo %d", ev);
    int k=metodPreklapanja(ev);
    Baket* bak =(Baket*)malloc(sizeof(Baket));
    FILE *f = fopen(naziv,"rb+");
    rewind(f); //visak uvek ali neka
    fseek(f,(k-1)*sizeof(Baket),SEEK_SET);
    fread(bak,sizeof(Baket),1,f);
    fclose(f);
    int indikator = 1;
   // printf("Adrugonulti %d\n", bak->nizSlogova[0].ebr);
    for(int i=0;i<fb;i++){
       // printf("mjedan\n");
        //printf("Adrugo %d\n", bak->nizSlogova[i].ebr);
        if(bak->nizSlogova[i].ebr==ev){
            //printf("jedan\n");
            return 1;
        }

    }
   // printf("dva\n");
    if(bak->adresa==-1){
        //printf("tri\n");
        return 0;
    }
    //printf("cetri\n");
    char ime[22];
    strcpy(ime,naziv);
    strcat(ime,"-zonaprekoracenja");
    SlogPrekoracioc* slow = (SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
    int adresa=bak->adresa;
    FILE* fp =fopen(ime,"rb+");
    fseek(fp,sizeof(int),SEEK_SET);
    fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
    fread(slow,sizeof(SlogPrekoracioc),1,fp);
    if(slow->ebr==ev){
        //printf("pet\n");
        fclose(fp);
        return 1;
    }
    //printf("sest\n");
    while(adresa!=-1){
        //printf("sedam\n");
        if(slow->ebr==ev){
           // printf("osam\n");
            fclose(fp);
            return 1;

        }
        //printf("devet\n");
        adresa=slow->sledeci;
        if(adresa!=-1){
            //printf("deset\n");
            fread(slow,sizeof(SlogPrekoracioc),1,fp);
        }
        //printf("jedanaest\n");

    }
    //printf("dvanaest\n");
    fclose(fp);
    return 0;
    //printf("trinaest\n");

//vraca 0 ako takav slog ne postoji, inace !=0
}


void upisNovogSloga(char *naziv){
    Slog *trenutni = (Slog *)malloc(sizeof(Slog));
    SlogPrekoracioc* sp =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
    sp->sledeci=-1;
    Baket * preuzetBaket =(Baket*)malloc(sizeof(Baket));
    
    printf("Dodavanje novog sloga: \n");
    printf("Unesite potrebna polja: \n");
    do{
    printf("Evidencioni broj(9 cifara): ");
    scanf("%d",&trenutni->ebr);
    }while(trenutni->ebr<100000000 || trenutni->ebr>999999999);
    printf("\n");
if(!trazenjeSloga(trenutni->ebr,naziv)){   //neuspesno trazenje
    /*do{
    printf("Lični broj psa (11 cifara/karaktera): ");
    scanf("%s",trenutni->id);
    }while(strlen(trenutni->id)!=11);
    do{
    printf("Datum i vreme u formatu [dd.mm.gggg.#ss:mm] : ");
    scanf("%s",trenutni->dateTime);
    }while(strlen(trenutni->dateTime)!=17);
    do{
    printf("Tip pregleda (8 karaktera): ");
    scanf("%s",trenutni->type);
    }while(strlen(trenutni->type)!=8);

    do{
    printf("Trajanje pregleda u min (<1200 upis u rasutu, >1200 upis u serijsku): ");
    scanf("%d",&trenutni->trajanje);
    }while(trenutni->trajanje<0);*/
    printf("\n");
    printf("Dodavanje u toku...\n");
    
    int kljuc =metodPreklapanja(trenutni->ebr)-1;
    
    
    
    FILE * f=fopen(naziv,"rb+");
    fseek(f,kljuc*sizeof(Baket),SEEK_SET);
    fread(preuzetBaket,sizeof(Baket),1,f);
    fclose(f);
    int indikator=0;
    if(preuzetBaket->adresa==-1){
        //mozda u maticni mozda u zonu prekoracenja
        
        int j;
        for(int j=0;j<fb;j++){
            if(preuzetBaket->nizSlogova[j].ebr==-1){
                
                indikator=1;
                break;
            }
        }
        
        if(indikator){
            int k=0;
            for(k=0;k<fb;k++){
                if(preuzetBaket->nizSlogova[k].ebr==-1){
                    break;
                }
            }
            int ind2=1;
            for(int t=0;t<k;t++){
                if(preuzetBaket->nizSlogova[t].ebr==trenutni->ebr){
                    ind2=0;
                }
            }
            if(ind2){
            printf("Upisivanje u maticni baket...\n");
            preuzetBaket->nizSlogova[k].ebr=trenutni->ebr;
            preuzetBaket->nizSlogova[k].ia=1;
            FILE *d =fopen(naziv,"rb+");
            int key =metodPreklapanja(trenutni->ebr)-1;
            fseek(d,key*sizeof(Baket),SEEK_SET);
            fwrite(preuzetBaket,sizeof(Baket),1,d);
            fclose(d);

            //printf("K je %d\n",k);
            }else{
                printf("Ne mozete uneti slog sa vec postojecim evidencionim brojem!\n");
            }
           
        }else{
            puts("ipak u prekoracenje");
            char ime[22];
            strcpy(ime,naziv);
            strcat(ime,"-zonaprekoracenja");
            int adr;
            FILE* fp =fopen(ime,"rb+");
            rewind(fp);
            fread(&adr,sizeof(int),1,fp);
            sp->ebr=trenutni->ebr;
            sp->sledeci=-1;
            sp->citaj=1;
            //i sve ostalo
            rewind(fp);
            fseek(fp,sizeof(int),SEEK_SET);
            fseek(fp,adr*sizeof(SlogPrekoracioc),SEEK_CUR);
            fwrite(sp,sizeof(SlogPrekoracioc),1,fp);
            int adrr=adr+1;
            rewind(fp);
            fwrite(&adrr,sizeof(int),1,fp);
            fclose(fp);
            
            
            
            preuzetBaket->adresa=adr;
            FILE*tr=fopen(naziv,"rb+");
            fseek(tr,kljuc*sizeof(Baket),SEEK_SET);

            fwrite(preuzetBaket,sizeof(Baket),1,tr);
            fclose(tr);
           

        }



    }else{
        puts("radis upis u prekoracenej odmah");
        int pocetak;
        int sledecaSlobodna;
        pocetak=preuzetBaket->adresa;
        printf("Preuzeta adresa je: %d\n", pocetak);
        char ime2[22];
        strcpy(ime2,naziv);
        strcat(ime2,"-zonaprekoracenja");
        FILE* up = fopen(ime2,"rb+");
        rewind(up);
        fread(&sledecaSlobodna,sizeof(int),1,up);
        SlogPrekoracioc * trtr=(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
        
        int indxpre=pocetak;
        rewind(up);
        fseek(up,sizeof(int),SEEK_SET);
        fseek(up,pocetak*sizeof(SlogPrekoracioc),SEEK_CUR);
        fread(trtr,sizeof(SlogPrekoracioc),1,up);
        
        while(trtr->sledeci!=-1){
            
            indxpre=trtr->sledeci;
            rewind(up);
            fseek(up,sizeof(int),SEEK_SET);
            fseek(up,indxpre*sizeof(SlogPrekoracioc),SEEK_CUR);
            fread(trtr,sizeof(SlogPrekoracioc),1,up);
            
        }
        
        
        trtr->sledeci=sledecaSlobodna;
        rewind(up);
        fseek(up,sizeof(int),SEEK_SET);
        fseek(up,indxpre*sizeof(SlogPrekoracioc),SEEK_CUR);
        fwrite(trtr,sizeof(SlogPrekoracioc),1,up);
        sp->sledeci=-1;
        sp->ebr=trenutni->ebr;
        sp->citaj=1;
        rewind(up);
        fseek(up,sizeof(int),SEEK_SET);
        fseek(up,sledecaSlobodna*sizeof(SlogPrekoracioc),SEEK_CUR);
        fwrite(sp,sizeof(SlogPrekoracioc),1,up);
        int df = sledecaSlobodna+1;
        rewind(up);
        fwrite(&df,sizeof(int),1,up);
        fclose(up);
        free(trtr);
        
        




    }
    
    free(trenutni);
    free(preuzetBaket);
    free(sp);

  /*  char ime[22];
    strcpy(ime,naziv);
    strcat(ime,"-zonaprekoracenja");
    
    FILE* fp =fopen(ime,"rb+");
    fseek(fp,sizeof(int),SEEK_SET);
    fwrite(sp,sizeof(SlogPrekoracioc),1,fp);
    fclose(fp);*/
    
}else{
    printf("Greska!Slog vec postoji!");
}   
printf("Dodje do kraja\n");

}

void brisanje(int ev, char*naziv){
//brisanje se poziva onda kad je prethodilo uspesno trazenje, dakle
//u kombinaciji sa funkcjiom trazenjeSloga...
    printf("Brisanje... \n");
    Baket * bak = (Baket *)malloc(sizeof(Baket));
    int kljuc =metodPreklapanja(ev)-1;
    FILE *f=fopen(naziv,"rb+");   
    fseek(f,kljuc*sizeof(Baket),SEEK_SET);
    fread(bak,sizeof(Baket),1,f);
    int indikator=0;
    int i;
    for(int i=0;i<fb;i++){
            if(bak->nizSlogova[i].ebr==ev){
                indikator=1;
                break;
            }
        }
    
    if(indikator){
        /*bak->nizSlogova[i].ebr=-1;
        bak->nizSlogova[i].ia=0;
        bak->nizSlogova[i];
        bak->nizSlogova[i].ia=0;
         bak->nizSlogova[i].ebr=-1;
         strcpy(bak->nizSlogova[i].id, "xxxxxxxxxxx");
         strcpy(bak->nizSlogova[i].dateTime,"01.01.1974. 11:11");
         strcpy(bak->nizSlogova[i].type,"xxxxxxxx");
         bak->nizSlogova[i].trajanje=-1;
        FILE * fk =fopen(naziv,"rb+");
        rewind(fk);
        fseek(fk,kljuc*sizeof(Baket),SEEK_SET);
        fwrite(bak,sizeof(bak),1,fk);
        fclose(fk);*/
        
        if(bak->adresa!=-1){
            int i=0;
            for(i;i<fb;i++){
            if(bak->nizSlogova[i].ebr==ev){
                indikator=1;
                break;
                }
            }
            printf("U1**\n");
            //printf("%d aaa",i);
            SlogPrekoracioc* slogj =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
            char ime[22];
            strcpy(ime,naziv);
            strcat(ime,"-zonaprekoracenja");
            FILE* fp =fopen(ime,"rb+");
            fseek(fp,sizeof(int),SEEK_SET);
            fseek(fp,(bak->adresa)*sizeof(SlogPrekoracioc),SEEK_CUR);
            fread(slogj,sizeof(SlogPrekoracioc),1,fp);
            
            //bak->nizSlogova[1].ia=1;
            bak->nizSlogova[i].ebr=slogj->ebr;
            strcpy(bak->nizSlogova[i].id, slogj->id);
            strcpy(bak->nizSlogova[i].dateTime,slogj->dateTime);
            strcpy(bak->nizSlogova[i].type,slogj->type);
            bak->nizSlogova[i].trajanje=slogj->trajanje;
            slogj->citaj=0;
            fseek(fp,sizeof(int),SEEK_SET);
            fseek(fp,(bak->adresa)*sizeof(SlogPrekoracioc),SEEK_CUR);
            fwrite(slogj,sizeof(SlogPrekoracioc),1,fp);
            fclose(fp);
            if((slogj->sledeci)!=-1){
            bak->adresa=slogj->sledeci;
            }else{
                bak->adresa=-1;
            }
            
            FILE * fk =fopen(naziv,"rb+");
            rewind(fk);
            fseek(fk,(metodPreklapanja(ev)-1)*sizeof(Baket),SEEK_SET);
            fwrite(bak,sizeof(Baket),1,fk);
            fclose(fk);
            free(slogj);
            free(bak);
            
            
        


        }else{
            printf("U2**\n");
            bak->nizSlogova[i].ia=0;
            bak->nizSlogova[i].ebr=-1;
            strcpy(bak->nizSlogova[i].id, "xxxxxxxxxxx");
            strcpy(bak->nizSlogova[i].dateTime,"01.01.1974. 11:11");
            strcpy(bak->nizSlogova[i].type,"xxxxxxxx");
            bak->nizSlogova[i].trajanje=-1;
            FILE * fk =fopen(naziv,"rb+");
            rewind(fk);
            fseek(fk,kljuc*sizeof(Baket),SEEK_SET);
            fwrite(bak,sizeof(bak),1,fk);
            fclose(fk);
        }

    
    }else{
        //slog nije u maticnom baketu
        printf("U2**\n");
            int adresa =bak->adresa;
            SlogPrekoracioc* slogn =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
            SlogPrekoracioc* slogj =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
            char ime[22];
            strcpy(ime,naziv);
            strcat(ime,"-zonaprekoracenja");
            FILE* fp =fopen(ime,"rb+");
            fseek(fp,sizeof(int),SEEK_SET);
            fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
            fread(slogn,sizeof(SlogPrekoracioc),1,fp);
            fseek(fp,-sizeof(SlogPrekoracioc),SEEK_CUR);
            fread(slogj,sizeof(SlogPrekoracioc),1,fp);
            rewind(fp);
            fseek(fp,sizeof(int),SEEK_SET);
            if(slogn->sledeci!=-1){
            fseek(fp,slogn->sledeci*sizeof(SlogPrekoracioc),SEEK_CUR);
            fread(slogj,sizeof(SlogPrekoracioc),1,fp);
            
            while(slogj->ebr!=ev){
                rewind(fp);
                fseek(fp,sizeof(int),SEEK_SET);
                fseek(fp,slogj->sledeci*sizeof(SlogPrekoracioc),SEEK_CUR);
                fread(slogj,sizeof(SlogPrekoracioc),1,fp);
                adresa=slogn->sledeci;
                rewind(fp);
                fseek(fp,sizeof(int),SEEK_SET);
                fseek(fp,slogn->sledeci*sizeof(SlogPrekoracioc),SEEK_CUR);
            }
            slogn->sledeci=slogj->sledeci;
            free(slogj);
            slogn->citaj=0;
            rewind(fp);
            fseek(fp,sizeof(int),SEEK_SET);
            fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
            fwrite(slogn,sizeof(SlogPrekoracioc),1,fp);
            fclose(fp);
            free(slogj);
            }else{
                printf("U4**\n");
                bak->adresa=-1;
                FILE * ff =fopen(naziv,"rb+");
                fseek(f,kljuc*sizeof(Baket),SEEK_SET);
                fwrite(bak,sizeof(Baket),1,ff);
                fclose(ff);
                free(slogn);
                free(slogj);



            }


    }



free(bak);




}




void ispisSloga(int ev, char * naziv){
//poziva se samo ako slog vec postoji (dakle uz kombinaciju sa trezenjeSloga)
    printf("/////////////////////////////////////////");
    printf("Ispis trazenog sloga: \n");
    Baket * bak = (Baket *)malloc(sizeof(Baket));
    int kljuc =metodPreklapanja(ev)-1;
    FILE *f=fopen(naziv,"rb+");
    
    fseek(f,kljuc*sizeof(Baket),SEEK_SET);
    fread(bak,sizeof(Baket),1,f);
    fclose(f);
    int indikator=0;
    for(int i=0;i<fb;i++){
            if(bak->nizSlogova[i].ebr==ev){
                indikator=1;
                break;
            }
        }
    
    if(indikator){
        int i;
        for(int i=0;i<fb;i++){
            if(bak->nizSlogova[i].ebr==ev){
                break;
            }
        }
        printf("%d-%d slog:\n",kljuc,i+1);
        printf("Aktuelan slog: ");
        if(bak->nizSlogova[i].ia){
            printf("DA\n");
        }else{
            printf("NE\n");
        }
        printf("Evidencioni broj: %d\n",bak->nizSlogova[i].ebr);
        printf("Lični broj psa: \n");
        puts(bak->nizSlogova[i].id);
        printf("Datum i vreme pregleda: ");
        puts(bak->nizSlogova[i].dateTime);
        printf("Oznaka vrste pregleda: ");
        puts(bak->nizSlogova[i].type);
        printf("Trajanje pregleda: %d min\n", bak->nizSlogova[i].trajanje);
        printf("------------------------------------\n");
    }else{
        int adresa =bak->adresa;
        SlogPrekoracioc* slogm =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
        char ime[22];
        strcpy(ime,naziv);
        strcat(ime,"-zonaprekoracenja");
    
        FILE* fp =fopen(ime,"rb+");
        fseek(fp,sizeof(int),SEEK_SET);
        fread(slogm,sizeof(SlogPrekoracioc),1,fp);
        while(slogm->ebr!=ev){
            fread(slogm,sizeof(SlogPrekoracioc),1,fp);
        }
        if(slogm->citaj){
        printf("Maticni baket: %d\n",metodPreklapanja(slogm->ebr));
        printf("Evidencioni broj: %d\n",slogm->ebr);
        printf("Evidencioni broj psa: ");
        puts(slogm->id);
        printf("Datum i vreme: ");
        puts(slogm->dateTime);
        printf("Vrsta pregleda: ");
        puts(slogm->type);
        printf("Trajanje pregleda: %d min\n", slogm->trajanje);
        if(slogm->sledeci!=-1){
        printf("Adresa sledeceg sinonima: %d\n", slogm->sledeci);
        }else{
        printf("Adresa sledeceg sinonima: Nema vise!\n");
        }
        printf("------------------------------------\n");
        fclose(fp);
        free(slogm);
        }

    }
free(bak);

}
void promeni(int ev, char *naziv){
    Slog* sg=(Slog*)malloc(sizeof(Slog));
    do{
    printf("Tip pregleda (8 karaktera): ");
    scanf("%s",sg->type);
    }while(strlen(sg->type)!=8);

    Baket * bak = (Baket *)malloc(sizeof(Baket));
    int kljuc =metodPreklapanja(ev)-1;
    FILE *f=fopen(naziv,"rb+");
    
    fseek(f,kljuc*sizeof(Baket),SEEK_SET);
    fread(bak,sizeof(Baket),1,f);
    fclose(f);
    int indikator=0;
    int i=0;
    for(i;i<fb;i++){
            if(bak->nizSlogova[i].ebr==ev){
                indikator=1;
                break;
            }
        }
    if(indikator){
        strcpy(bak->nizSlogova[i].type,sg->type);
        FILE * f =fopen(naziv,"rb+");
        rewind(f);
        fseek(f,kljuc*sizeof(Baket),SEEK_SET);
        fwrite(bak,sizeof(Baket),1,f);
        fclose(f);
        //free(bak);




    }else{
        SlogPrekoracioc * s =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
        char ime[22];
        int adresa=bak->adresa;
        strcpy(ime,naziv);
        strcat(ime,"-zonaprekoracenja");
        FILE * fp =fopen(ime,"rb+");
        rewind(fp);
        fseek(fp,sizeof(int),SEEK_CUR);
        fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
        fread(s,sizeof(SlogPrekoracioc),1,fp);
        
        while(s->ebr!=ev){
            rewind(fp);
            fseek(fp,sizeof(int),SEEK_CUR);
            fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
            adresa=s->sledeci;
        }
        strcpy(s->type,sg->type);
        rewind(fp);
        fseek(fp,sizeof(int),SEEK_CUR);
        fseek(fp,adresa*sizeof(SlogPrekoracioc),SEEK_CUR);
        fwrite(s,sizeof(SlogPrekoracioc),1,fp);
        fclose(fp);
        free(s);





    }

free(bak);


}


void ispisiDatoteku(char *naziv){
    FILE *f = fopen(naziv,"rb+");
    printf("\n**************************Ispis datoteke*****************************\n");
    Baket *b = (Baket *)malloc(sizeof(Baket));
    for(int i=0;i<B;i++){
        
        rewind(f);
        
        fseek(f,i*sizeof(Baket),SEEK_CUR);
        //Baket *b = (Baket *)malloc(sizeof(Baket));
        fread(b,sizeof(Baket),1, f);
        for(int j=0; j<fb;j++){
            
            printf("%d-%d slog:\n",i+1,j+1);
            printf("Aktuelan slog: ");
            if(b->nizSlogova[j].ia){
                printf("DA\n");
            }else{printf("NE\n");}
            printf("Evidencioni broj: %d\n",b->nizSlogova[j].ebr);
            printf("Lični broj psa: \n");
            puts(b->nizSlogova[j].id);
            printf("Datum i vreme pregleda: ");
            puts(b->nizSlogova[j].dateTime);
            printf("Oznaka vrste pregleda: ");
            puts(b->nizSlogova[j].type);
            printf("Trajanje pregleda: %d min\n", b->nizSlogova[j].trajanje);
            printf("------------------------------------\n");

        }
        printf("Pokazivac: ");
        printf("%d\n", b->adresa);
        //free(b);
        printf("************************************************************\n");
        
    }free(b);
    //fclose(f);
    fclose(f);
   printf("************************************************************\n");
    printf("Zona prekoracenja: \n");
    char ime[22];
    strcpy(ime,naziv);
    strcat(ime,"-zonaprekoracenja");
    int kraj;
    SlogPrekoracioc *slogm =(SlogPrekoracioc*)malloc(sizeof(SlogPrekoracioc));
    FILE* fp =fopen(ime,"rb+");
    rewind(fp);
    fread(&kraj,sizeof(int),1,fp);
    for(int i=0;i<kraj;i++){
        int k=i;
        fread(slogm,sizeof(SlogPrekoracioc),1,fp);
        if(!slogm->citaj){
            k--;
        }
        if(slogm->citaj){
        printf("--------------------------\n");
        printf("prekoracioc: \n");
        printf("Maticni baket: %d\n",metodPreklapanja(slogm->ebr));
        printf("Evidencioni broj: %d\n",slogm->ebr);
        printf("Evidencioni broj psa: ");
        puts(slogm->id);
        printf("Datum i vreme: ");
        puts(slogm->dateTime);
        printf("Vrsta pregleda: ");
        puts(slogm->type);
        printf("Trajanje pregleda: %d min\n", slogm->trajanje);
        if(slogm->sledeci!=-1){
        printf("Adresa sledeceg sinonima: %d\n", slogm->sledeci);
        }else{
        printf("Adresa sledeceg sinonima: Nema vise!\n");
        }
        }

    }printf("************************************************************\n");
    fclose(fp);
    free(slogm);

    
     
  


}

void meni(aktivnaDatoteka *ad){
   int k;
 
    do{
        printf("************************************************************\n");
        printf("Izaberite odgovarajucu opciju upisivanjem r.br.: \n");
        printf("1.formiranje prazne datoteke\n");
        printf("2.izbor aktivne datoteke zadavanjem njenog naziva\n");
        printf("3.prikaz naziva aktivne datoteke\n");
        printf("4.upis novog sloga u aktivnu datoteku\n");
        printf("5.traženje sloga u aktivnoj datoteci i njegov prikaz\n");
        printf("6.prikaz svih slogova aktivne datoteke\n");
        printf("7.fizičko brisanje aktuelnog sloga iz aktivne datoteke\n");
        printf("8.promena vrste pregleda u odabranom slogu iz aktivne datoteke\n");
        printf("0.zavrsetak programa\n");
        //printf("************************************************************\n");
        printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
        scanf("%d", &k);
        printf("\n");

    
    
    switch(k){
        case 0:
        exit(1);
        break;
        case 1:
            printf("Formiranje prazne datoteke.\n");
            char a[21];
            printf("\nUnesite naziv datoteke: \n");
            scanf("%s",a);
            //dodati da ne napravi opet istu datoteku2
            strcpy(ad->naziv,a);
            printf("\n");
            formirajDatoteku(ad->naziv);
            break;
        case 2:
            
            printf("Postavite datoteku aktivnom zadavanjem njenog naziva: \n");
            char b[21];
            printf("\nUnesite naziv datoteke: \n");
            scanf("%s",b);
            /*if(ad->f!=NULL){
                fclose(ad->f);
                //fclose(imePrekoracenje(ad->naziv));
            }*/
            if(access(b, F_OK ) != -1 ) {
                 strcpy(ad->naziv,b);
                //FILE* fg = fopen(b,"rb+");
                //ad->f=fg;
                ad->ind=1;
                 printf("Uspesno postavljanje aktivne datoteke!\n");
                 printf("Da li postoji slog 500040000 %d\n",trazenjeSloga(500040000,ad->naziv));
                 printf("800000100 %d\n", trazenjeSloga(800000100,ad->naziv));
                 printf("601001000 %d", trazenjeSloga(601001000,ad->naziv) );

            } else {
                printf("Greska! Fajl ne postoji.\n");
                //ad->f=NULL;
                strcpy(ad->naziv," ");
                break;
            }
            printf("\n");
            break;

        case 3:
            printf("Trenutno aktivna datoteka je: \n");
            
            if(ad->ind==0){
                printf("Nije postavljena aktivna datoteka!\n");
            }else{
                puts(ad->naziv);
            }
             break;
        case 4:
            if(ad->ind!=0){
            upisNovogSloga(ad->naziv);
            }else{
                printf("Greska verovatno datoteka nije izabrana !\n");
                break;
            }
            break;
        case 5:
            if(ad->ind!=0){
            int e=-1;

            do{
            printf("Unesite evidencioni broj sloga kojeg trazite: \n");
            scanf("%d",&e);
            }while(e<10000000||e>999999999);
            if(trazenjeSloga(e, ad->naziv)){
               ispisSloga(e,ad->naziv);
            }else{
               printf("Slog sa tim ev.brojem ne postoji!\n");

            }
            }else{
                printf("Nije izabrana aktivna datoteka!");
            }
            break;

        
        case 6:
            if(ad->ind!=0){
            ispisiDatoteku(ad->naziv);
            }else{
                printf("Nije izabrana aktivna datoteka!");
            }
            break;
        case 7:
            if(ad->ind!=0){
            int e=-1;

            do{
            printf("Unesite evidencioni broj sloga koji zelite da obrisete: \n");
            scanf("%d",&e);
            }while(e<10000000||e>999999999);
            if(trazenjeSloga(e, ad->naziv)){
               brisanje(e,ad->naziv);
            }else{
               printf("Slog sa tim ev.brojem ne postoji!\n");

            }
            }else{
                printf("Nije izabrana aktivna datoteka!");
            }
            break;
        case 8:
            
            if(ad->ind!=0){
            int e=-1;

            do{
            printf("Unesite evidencioni broj sloga kom zelite da promenite vrstu pregleda: \n");
            scanf("%d",&e);
            }while(e<10000000||e>999999999);
            if(trazenjeSloga(e, ad->naziv)){
               
               promeni(e,ad->naziv);
            }else{
               printf("Slog sa tim ev.brojem ne postoji!\n");

            }
            }else{
                printf("Nije izabrana aktivna datoteka!");
            }
            
            break;
        
        default:
            break;
    }
    }while(1);
}
#endif