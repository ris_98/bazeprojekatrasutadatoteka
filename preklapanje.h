#ifndef PREKLAPANJE_H_INCLUDED
#define PREKLAPANJE_H_INCLUDED

#include "math.h"  //pozovi -lm kad kompajliras uvek zaboravis!!!!!!!

#define B 7   //broj baketa
#define p 9   //broj cifara kljuca, u ovom slucaju 9 evidencioni broj
#define n 1   //broj cifara relativne adrese
#define q 4   //donje celo p/2n
#define v 10  //osnova brojnog sistema

int metodPreklapanja(int x){
    int niz[p];
    int suma1 = 0;
    int suma2 = 0;
    int suma;
    int cifra;
    int i = 0;
    while (x > 0) {
        cifra = x % 10;
        niz[i]=cifra;
        i++;
        x /= 10;
    }
    int r;
    int cr;
    int vv=1;
    for(int k=0; k<=q;k++){
        for(int i=0;i<n;i++){
            r=2*k*n+i;
            if(r<p){
                cr = niz[r-1];
            }else{
                cr=0;
            }
            suma1=suma1+cr*vv;
            vv=pow(v,i);
        }
    }
    int s;
    int cs;
    vv=1;
    for(int k=1;k<=q;k++){
        for(int i=0; i<n;i++){
            s=2*k*n-i-1;
            if(s<p){
                cs = niz[s-1];
            }else{
                cs=0;
            }
            suma2=suma2+cs*vv;
            vv=pow(v,i);

        }
    }
    suma=suma1+suma2;
    //ovde recimo kad pokusam da kastujem sumu u double izbacuje broj koji nema smisla
    int h = pow(v,n);
    suma=suma%h;
    suma=suma*B/h;
    return suma+1;        //reseno dodavanjem jedinice jer deljenje int na kraju radi floor,
}                         //tako da je opseg od 1 do B



#endif